import 'dart:collection';

void main() {
  Map<int, dynamic> mtnsince2012winningapps = {
    0: "FNB 2012",
    1: "FNB 2013",
    2: "AAA 2012",
    3: "ORDERIN 2017",
    4: "COWABUNGA 2018"
  };

  print(mtnsince2012winningapps);

  //sort apps by app name

  var sortByAppName = new SplayTreeMap<int, String>.from(
      mtnsince2012winningapps,
      (key1, key2) => mtnsince2012winningapps[key1]
          .compareTo(mtnsince2012winningapps[key2]));
  print("***Sorted Map by App name***");

  print(sortByAppName.values);

// Print the winning app of 2017.

  mtnsince2012winningapps.forEach((key, value) {
    if (value.contains("2017")) {
      print("The winning app of 2017 is $value");
    }
  });

//Print the winning app of 2018.

  mtnsince2012winningapps.forEach((key, value) {
    if (value.contains("2018")) {
      print("The winning app of 2018 is  $value");
    }
  });

//Print total number of apps from the array.
  var Printtotalnumberofappsfromthearray = mtnsince2012winningapps.length;

  print("Total number of apps in array is $Printtotalnumberofappsfromthearray");
}
